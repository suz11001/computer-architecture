/*
 * riscy-uconn simulator.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "simulator.h"
#include "mipsInstructionMap.h"

int debug = 0;

int main(int argc, char *argv[]) {

  if (argc != 2) {
    printf("Incorrect number of arguments\n");
    exit(1);
  }
  else {
    // Open Input file
    FILE *fp;
    fp = fopen(argv[1], "r");
    
    // Call initialize function for registers and instruction/data memory
    initialize(fp);

    // Process one instruction at a time
    process_instructions();
 
    printf("\n Printing Output Registers \n");

    // Output registers
    rdump();

    // Output Memory
    mdump();

    // Dealloc .data
    free(memory);
    memory = NULL;

    // Close File
    fclose (fp);
    return 0;
  }
}

// Function to take in instruciton to parse
void process_instructions() {

   int terminate = 0;
   // Cycle PC
   int instruction_counter = 0;

   while(terminate != 1)
   {
     // set terminate flag when SLL $0, $0, 0 (aka NOOP) is executed
     if (memory[pc/4] == 0)
        terminate = 1;
     
     //Fetch Instruction
     unsigned int instruction_fetch;
     instruction_fetch = fetch();
     if(debug==1)
        printf("\n IMEM ADDR:%d instruction 0x%08x", pc/4, instruction_fetch);
     instruction_counter++;

     // Decode Instruction
     struct Registers decode_out;
     decode_out = decode(instruction_fetch);

     // Execute Instruction
     struct Registers alu_out;
     alu_out = execute(decode_out);

     // Memory Stage
     struct Registers mem_out;
     mem_out = memory_stage(alu_out);

     // Write Back Stage
     write_back_stage(mem_out);
   }//while pc loop
   printf("\n TOTAL INSTRUCTIONS EXECUTED: %d", instruction_counter);
   rdump();
   mdump();
} //process_instruction

// Advance PC
void advance_pc(int step) {
  pc += step;
}

unsigned int fetch() {
  unsigned int inst = 0;
  return inst = memory[pc/4];
}

struct Registers decode(unsigned int instruction_fetch) {
  	struct Registers reg_temp;
  
  	// your code for decode phase goes here.
  
  	//shift the instruction 26 bits to the right and get the op code
  	reg_temp.x = instruction_fetch >> 26;
  	printf("***********op code %d\n",reg_temp.x);
  	//if op code is of an r type instruction, fetch the funct and the registers
  	if (reg_temp.x == RTYPEOP) {
  		printf("\n it's a r type instruction\n");
  		//shift the instruction 26 bits to the left to isolate funct and shift again to extract
  		unsigned int y =  instruction_fetch << 26;
  		y = (y >> 26); // y holds the decimal for the function of the instruction
  		reg_temp.y = y;
  		printf("function is %d\n",reg_temp.y);
		//if we need to jump registers, we won't have an rd, rt or sa
		if (y==JR){
			printf("\n we need to jump registers");
			int reg = (instruction_fetch >> 21) & 0x1F;
			reg_temp.rs=reg;
			printf("\n extract from this register %d", reg_temp.rs);
			printf("\n need to jump to:%d",registers[reg_temp.rs]);
			pc = registers[reg_temp.rs];
			printf("\n we need to jump registers");
		}
		//otherwise extract the rs, rd, and rt values
		else{
			reg_temp.rs = (instruction_fetch >> 21) & 0x1F;
			reg_temp.rt = (instruction_fetch >> 16) & 0x1F;
			reg_temp.rd = (instruction_fetch >> 11) & 0x1F;
			reg_temp.sa = (instruction_fetch >> 6) & 0x1F;
			advance_pc(4);
		}
  	}
  	// otherwise it's an I type op
  	else if (reg_temp.x != J && reg_temp.x != JAL) { 
  		printf("\n it's a i type instruction");
		// Get rs, rt, immediate for an i type instruction
		reg_temp.rs = (instruction_fetch >> 21) & 0x1F;
		reg_temp.rt = (instruction_fetch >> 16) & 0x1F;
		reg_temp.imm = instruction_fetch & 0xFFFF;
		//if y is not BNE BEQ then advance pc by 4
		if (reg_temp.x!=BNE && reg_temp.x!=BEQ ){
			advance_pc(4);
		}
		// beq rs, rt, label
		else if (reg_temp.x == BEQ) {
			printf("\n beq instruction"); 
			if (registers[reg_temp.rs] == registers[reg_temp.rt]) {
				signed short int offset = instruction_fetch & 0xFFFF;
				advance_pc(4 + (offset)); //--> works
				printf("\n program counter:%d", pc);
			}
			else{
				advance_pc(4);
			}
		}

		else if (reg_temp.x == BNE) {
			printf("\n bne instruction"); 
			if (registers[reg_temp.rs] != registers[reg_temp.rt]) {
				signed short int offset = instruction_fetch & 0xFFFF;
				advance_pc(4 + (offset));//--> works
				printf("\n program counter:%d", pc);
			}	
			else{
				advance_pc(4);
			}
		}
		else{
			printf("\n this should never print..."); 
		}
	}
	// else it a j type op
	else {
		printf("\n it's a j type instruction\n");
		// j immediate
		if (reg_temp.x == J){
			pc = instruction_fetch & 0x03FFFFFF;
			printf("program counter changed to: %d \n", pc);
		}
		// jal immediate
		else if (reg_temp.x==JAL) {
			reg_temp.jmp_out_31 = pc + 4;
			pc = instruction_fetch & 0x03FFFFFF;
			printf("program counter changed to: %d \n", pc);
		}
	}

	return reg_temp;
}


struct Registers execute(struct Registers decode_out) {

   	// your code for execute phase goes here.
   	//if you have an r type op
  	if (decode_out.x==RTYPEOP){
  	  	if (decode_out.y == ADD){
  	  		printf("rs %d\n",decode_out.rs);
  	  		printf("rt %d\n",decode_out.rt);
  			decode_out.C = registers[decode_out.rs] + registers[decode_out.rt];
  			printf("output %d\n",decode_out.C);
		}
		else if (decode_out.y == SUB){
			decode_out.C = registers[decode_out.rs] - registers[decode_out.rt];
		}
		else if (decode_out.y  == AND) {
			decode_out.C = registers[decode_out.rs] & registers[decode_out.rt];
		}
		// or rd, rs, rt
		else if (decode_out.y == OR) {
			decode_out.C = registers[decode_out.rs] | registers[decode_out.rt];		
		}
		// slt rd, rs, rt
		else if (decode_out.y == SLT) {
			if (registers[decode_out.rs] < registers[decode_out.rt]){
				decode_out.C = 1;
			}
			else{
				decode_out.C = 0;
			}		
		}
		// sll rd, rt, sa
		else if (decode_out.y == SLL) {
			decode_out.C = registers[decode_out.rt] << decode_out.sa;
		}
		// srl rd, rt, sa
		else if (decode_out.y == SRL) {
			decode_out.C = registers[decode_out.rt] >> decode_out.sa; 
		}
	}
	//i type ops
	//lw rt, immediate(rs)
	else if (decode_out.x == LW) {
		decode_out.addr_mem = registers[decode_out.rs] + decode_out.imm;
		printf("memory address is: %d\n",decode_out.addr_mem);
		decode_out.B = memory[decode_out.addr_mem];
	}

	// sw rt, immediate(rs)
	else if (decode_out.x == SW) {
		decode_out.addr_mem = registers[decode_out.rs] + decode_out.imm;
		decode_out.B = registers[decode_out.rt];
	}

	// andi rt, rs, immediate
	else if (decode_out.x == ANDI) {
		decode_out.addr_mem = registers[decode_out.rs] + decode_out.imm;
		decode_out.B = registers[decode_out.rs] & decode_out.imm;
	}

	// ori rt, rs, immediate
	else if (decode_out.x == ORI) {
		decode_out.B = registers[decode_out.rs] | decode_out.imm;
	}

	// slti rt, rs, immediate
	else if (decode_out.x == SLTI) {
		if (registers[decode_out.rs] < decode_out.imm)
			decode_out.B = 1;
		else
			decode_out.B = 0;
	}

	// addi rt, rs, immediate
	else if (decode_out.x == ADDI) {
		decode_out.B = registers[decode_out.rs] + decode_out.imm;
		printf("rs %d\n",registers[decode_out.rs]);
  	  	printf("immediate %d\n",decode_out.imm);
  	  	printf("output %d\n",decode_out.B);
	}

	// lui rt, immediate
	else if (decode_out.x == LUI) {
		decode_out.B = decode_out.imm << 16;
	}
	
	return decode_out;
}


struct Registers memory_stage(struct Registers alu_out)
{
  	// your code for memory phase goes here.
  	if (alu_out.x==LW ){
  		alu_out.B = memory[alu_out.addr_mem];
  		printf("stored in the memory is the value %d \n", alu_out.B);
  		mdump();
  	}
	else if (alu_out.x==SW){
		memory[alu_out.addr_mem] = alu_out.B;
		mdump();
	} 	
	else{
		printf("\n there is no memory stage for this instruction \n");
	}
  	return alu_out;
}

void write_back_stage(struct Registers mem_out)
{
   	// your code for write back phase goes here.
   	
   	//it's an r-type op, and not jr, the output is stored in C, so write it back to the rd register 
   	if (mem_out.x==RTYPEOP && mem_out.y!=JR){
		registers[mem_out.rd]=mem_out.C;
		rdump();
	}
	//it's an i-type op and not (BNE, BEQ, J, JAL, or SW), so the output for the rt registered is stored B 
	else if (mem_out.x!=BNE && mem_out.x!=BEQ && mem_out.x!=J && mem_out.x!=JAL && mem_out.x!=SW){
		registers[mem_out.rt]=mem_out.B;
		rdump();
	}
	// the write back for jal
	else if (mem_out.x==JAL ){
		printf("\n return to %d",mem_out.jmp_out_31);
		registers[31] = mem_out.jmp_out_31;
		rdump();
	}
	else{
		printf("\n there is no write back stage for this instruction \n");
	}
	
	//rdump();
} // WB Done


// Output reigsters
void rdump()
{
    printf("$0      $zero    0x%08x\n", registers[0]);
    printf("$1      $at      0x%08x\n", registers[1]);
    printf("$2      $v0      0x%08x\n", registers[2]);
    printf("$3      $v1      0x%08x\n", registers[3]);
    printf("$4      $a0      0x%08x\n", registers[4]);
    printf("$5      $a1      0x%08x\n", registers[5]);
    printf("$6      $a2      0x%08x\n", registers[6]);
    printf("$7      $a3      0x%08x\n", registers[7]);
    printf("$8      $t0      0x%08x\n", registers[8]);
    printf("$9      $t1      0x%08x\n", registers[9]);
    printf("$10     $t2      0x%08x\n", registers[10]);
    printf("$11     $t3      0x%08x\n", registers[11]);
    printf("$12     $t4      0x%08x\n", registers[12]);
    printf("$13     $t5      0x%08x\n", registers[13]);
    printf("$14     $t6      0x%08x\n", registers[14]);
    printf("$15     $t7      0x%08x\n", registers[15]);
    printf("$16     $s0      0x%08x\n", registers[16]);
    printf("$17     $s1      0x%08x\n", registers[17]);
    printf("$18     $s2      0x%08x\n", registers[18]);
    printf("$19     $s3      0x%08x\n", registers[19]);
    printf("$20     $s4      0x%08x\n", registers[20]);
    printf("$21     $s5      0x%08x\n", registers[21]);
    printf("$22     $s6      0x%08x\n", registers[22]);
    printf("$23     $s7      0x%08x\n", registers[23]);
    printf("$24     $t8      0x%08x\n", registers[24]);
    printf("$25     $t9      0x%08x\n", registers[25]);
    printf("$26     $k0      0x%08x\n", registers[26]);
    printf("$27     $k1      0x%08x\n", registers[27]);
    printf("$28     $gp      0x%08x\n", registers[28]);
    printf("$29     $sp      0x%08x\n", registers[29]);
    printf("$30     $fp      0x%08x\n", registers[30]);
    printf("$31     $ra      0x%08x\n", registers[31]);
    
    printf("  --> pc  0x%08x\n", pc);
}

// Output Memory
void mdump()
{
    FILE *fptr;
    fptr = fopen("mdump.txt","w");
    int i = 0;
    for(i=0;i<16384;i++)
    {
        fprintf(fptr,"\n Memory[%d] = %d",i,memory[i]);
    }
}

void initialize(FILE *fp)
{
    if (fp == NULL)
    {
      printf("Error opening input file.\n");
      exit(1);
    }

    // Initialize registers values' to 0x0
    for (int i = 0; i < 32; i++)
      registers[i] = 0x0;

    printf("\n Initialized Registers \n");

    // Malloc memory
    memory  = (int *)malloc(16384 * sizeof(int));
    if (memory == NULL)
    {
      printf("Not enough memory. Aborting..\n");
      exit(1);
    }

    // Initialize 'memory' array to -1
    for (int i = 0; i < 16384; i++)
    {
      memory[i] = -1;
    }

    printf("\n Initialized Memory \n");

    // Initialize variables for parsing
    char line[MAX_LENGTH+2];
    char *p;
    int i = 0, line_num = 0;
    //int line_num = 0, i = 0, data_line = 0;

    // Copy .text section to memory, breaks at nop
    while (fgets(line, MAX_LENGTH+2, fp) != NULL)
    {
      line_num++;

      // Remove '\n' from 'line'
      p = strchr(line, '\n');
      if (p != NULL)
        *p = '\0';

      memory[i] = getDec(line);
      //printf("\n %d",memory[i]);

      if (strcmp(line, "11111111111111111111111111111111") == 0)
      {
        memory[i] = 0;
        i = 0x800;
        break;
      }
      else
        i++;
    }

    int j = 2048; //Data Memory Starts at 2048
    for(j=i;j<16384;j++)
    {
      memory[j] = 2;
      //printf("\n %d",memory[i]);
    }

    printf("\n Instructions Read, Memory Offset: %d\n", line_num);

    // Seek fp to first instruction in .data
    char data[MAX_LENGTH+2];
    int bytes = 33 * line_num;
    fseek(fp, bytes, SEEK_SET);

    // Copy .data section to memory
    while (fgets(line, MAX_LENGTH+2, fp) != NULL)
    {
      // Remove '\n' from 'line'
      p = strchr(line, '\n');
      if (p != NULL)
        *p = '\0';

      memory[i] = getDec(line);
      printf("\n Data: %d %d",i,memory[i]);
      i++;
    }

    printf("\n Data put in Memory \n");
}

// Convert a binary string to a decimal value
int getDec(char *bin)
{
    int  b, k, m, n;
    int  len, sum = 0;
    
    // Length - 1 to accomodate for null terminator
    len = strlen(bin) - 1;
    
    // Iterate the string
    for(k = 0; k <= len; k++)
    {
        // Convert char to numeric value
        n = (bin[k] - '0');
        
        // Check the character is binary
        if ((n > 1) || (n < 0))
        {
            return 0;
        }
        
        for(b = 1, m = len; m > k; m--)
            b *= 2;
        
        // sum it up
        sum = sum + n * b;
    }
    return sum;
}

